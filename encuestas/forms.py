from django.forms import ModelForm, CharField, EmailField, Form
from django.contrib.formtools.wizard import FormWizard
from encuestas.models import Encuesta, Opciones
from django.http import HttpResponseRedirect

class EncuestasForm(ModelForm):
    pregunta = CharField(label='Pregunta de la encuesta') 
    autor = EmailField(label='Email del autor', error_messages={'invalid':"Este no es un correo.", 'required':'Este es un campo obligatoriamente obligado! '})
    class Meta: 
        model = Encuesta


class OpcionesForm(ModelForm):
    class Meta:
        model = Opciones
        exclude = ('encuesta', 'votos')

class Formu1(Form):
    nombre = CharField()

class Formu2(Form):
    nombre2 = CharField()

class EncuestasWizard(FormWizard):
    def get_template(self, step):
        return 'wizard.html'

    '''
    def process_step(self, request, form, step):
        import pdb
        #pdb.set_trace()
        pass
    '''

    def done(self, request, form_list):
        import pdb
        if request.method == 'POST':
            # Si el primer formulario es valido
            if form_list[0].is_valid():
                activa = False
                if form_list[0].cleaned_data['activa']:
                    activa = form_list[0].cleaned_data['activa'] 
                encuesta = Encuesta(pregunta=form_list[0].cleaned_data['pregunta'], activa=activa)
                encuesta.save()
            return HttpResponseRedirect('/')
            
        else:
            pass
