# -*- coding: UTF-8 -*-
from django.db import models

# Create your models here.
class Encuesta(models.Model):
    pregunta    = models.CharField(max_length=100)
    activa      = models.BooleanField()
    def __unicode__(self):
        return '%s' %(self.pregunta)

class Opciones(models.Model):
    opcion      = models.CharField(max_length=100, verbose_name=u'opción')
    votos       = models.IntegerField(default=0)
    encuesta    = models.ForeignKey('Encuesta')
    class Meta:
        verbose_name_plural = 'opciones'
