from encuestas.models import Encuesta, Opciones
from django.contrib import admin

class OpcionesInlineAdmin(admin.TabularInline):
    model = Opciones
    extra = 1

class EncuestaAdmin(admin.ModelAdmin):
    list_display = ('pregunta','activa',)
    inlines = [OpcionesInlineAdmin]
admin.site.register(Encuesta, EncuestaAdmin)

class OpcionesAdmin(admin.ModelAdmin):
    list_display = ('opcion','votos','encuesta',)
    list_filter  = ('encuesta__pregunta','encuesta__activa',)
    exclude = ('votos',)
admin.site.register(Opciones, OpcionesAdmin)
