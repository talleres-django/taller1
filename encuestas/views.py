# Create your views here.
from encuestas.models import Encuesta, Opciones
from django.shortcuts import render_to_response
from encuestas.forms import EncuestasForm, OpcionesForm
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required, permission_required

def listar_encuestas(request):
    encuestas = Encuesta.objects.filter(activa = True)
    return render_to_response('listar.html', {'encuestas': encuestas})

def encuesta(request, encuesta_id):
    opciones = Opciones.objects.filter(encuesta__id = encuesta_id)
    return render_to_response('votar.html', {'opciones': opciones, 'pregunta': opciones[0].encuesta.pregunta})
    
def votar(request, encuesta_id, opcion_id):
    opcion = Opciones.objects.get(id=opcion_id)
    opcion.votos = opcion.votos + 1
    opcion.save()
    
    opciones = Opciones.objects.filter(encuesta__id = encuesta_id)
    return render_to_response('resultados.html', {'opciones': opciones, 'pregunta': opciones[0].encuesta.pregunta})
    
@login_required
@permission_required('encuestas.add_encuesta')
def crear_encuesta(request):
    diccionario = {}
    diccionario.update(csrf(request))
    
    if request.method == 'POST':
		# Procesar el formulario
        form_crear_encuesta = EncuestasForm(request.POST)
        if form_crear_encuesta.is_valid():
            activa = False
            if request.POST.get('activa'):
                activa = request.POST['activa'] 
            encuesta = Encuesta(pregunta=request.POST['pregunta'], activa=activa)
            encuesta.save()
            
            return HttpResponseRedirect('/encuestas/crear_opciones/%d/' %(encuesta.id))
    else:
		# Mostrar el formulario
        form_crear_encuesta = EncuestasForm()
    
    diccionario.update({'form':form_crear_encuesta})
    return render_to_response('formulario.html', diccionario)

def crear_opciones(request, encuesta_id):
    diccionario = {}
    diccionario.update(csrf(request))
    
    if request.method == 'POST':
		# Procesar el formulario
        if request.POST['opcion'] == '':
            return HttpResponseRedirect('/encuestas/crear')

        form_crear_opciones = OpcionesForm(request.POST)

        if form_crear_opciones.is_valid():
            encuesta = Encuesta.objects.get(id=encuesta_id)
            opciones = Opciones(opcion=request.POST['opcion'], encuesta=encuesta)
            opciones.save()
    else:
        # Mostrar el formulario
        form_crear_opciones = OpcionesForm(initial={'encuesta':encuesta_id})
    diccionario.update({'form':form_crear_opciones})
    diccionario.update({'encuesta_id':encuesta_id})

    return render_to_response('formulario_opciones.html', diccionario)

    
