from django.conf.urls.defaults import patterns, include, url
from django.views.generic.create_update import create_object, update_object
from django.views.generic.edit import UpdateView
from encuestas.models import Encuesta
from encuestas.forms import *

urlpatterns = patterns('',
    url(r'^$', 'encuestas.views.listar_encuestas'),
    url(r'^(\d+)/(\d+)', 'encuestas.views.votar'),
    url(r'^(\d+)$', 'encuestas.views.encuesta'),
    url(r'^crear_opciones/(\d+)/','encuestas.views.crear_opciones'),
    #url(r'^wizard/$', (EncuestasWizard([Formu1, Formu2]))),
    url(r'^wizard/', (EncuestasWizard([EncuestasForm, OpcionesForm]))),
    url(r'^crear_generico', create_object,{'model':Encuesta, 'template_name':'formulario.html', 'post_save_redirect':'/encuestas/crear_opciones/%(id)d/'}),
    url(r'^modificar/(?P<pk>\d+)/', UpdateView.as_view(model=Encuesta, template_name='formulario.html')),

    url(r'^crear','encuestas.views.crear_encuesta'),
)
