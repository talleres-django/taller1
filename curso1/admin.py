from django.contrib import admin
from curso1.models import Noticias

class NoticiasAdmin(admin.ModelAdmin):
    list_display = ('titular', 'autor', 'fecha', 'publicar') # Estas son las columnas del admin
    search_fields = ('titular', 'resumen', 'contenido', 'autor') # Campo de busqueda de registros
    list_filter = ('publicar', 'fecha') # Filtro de resultados del modelo

admin.site.register(Noticias, NoticiasAdmin)
