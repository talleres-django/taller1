from django.db import models

class Noticias(models.Model):
    titular 	= models.CharField(max_length=100)
    resumen 	= models.CharField(max_length=200)
    contenido 	= models.TextField()
    fecha 		= models.DateField()
    autor 		= models.CharField(max_length=10)
    publicar 	= models.BooleanField()
    
    class Meta:
        verbose_name_plural = 'Noticias'
    
    def __unicode__(self):
        return u'%s del autor: %s' %(self.titular, self.autor)
        # nota: s -> string, d -> enteros, f-> flotantes
