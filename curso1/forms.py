from django.forms import Form, CharField, EmailField, PasswordField

class LoginForm(Form):
   usuario = CharField()
   clave = PasswordField()
