from django.shortcuts import render_to_response
from curso1.models import Noticias

def holamundo(request):
    return render_to_response('plantilla1.html')

def listarnoticias(request):
    #noticias = Noticias.objects.all()
    noticias = Noticias.objects.filter(publicar = True)
    print noticias
    return render_to_response('noticias.html',{'diccionario_noticias':noticias})
    
def detalle_noticia(request, noticia_id):
    noticia = Noticias.objects.filter(id = noticia_id)
    if noticia.exists() == True:
        return render_to_response('noticia.html', {'noticia': noticia[0]})

