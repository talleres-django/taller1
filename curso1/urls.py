from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.auth.views import login, logout
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'curso1.views.holamundo'),
    # r  -> Expresiones regulares / regex
    # ^  -> Comienzo de cadena
    # $  -> Final de cadena
    # d  -> Numeros enteros
    # +  -> Al menos un digito 
    # () -> Determina el parametro en la funcion de la vista
    url(r'^noticias$', 'curso1.views.listarnoticias'),
    url(r'^noticias/(\d+)', 'curso1.views.detalle_noticia'),
    url(r'^login/', login, {'template_name':'login.html'}),
    url(r'^logout/', logout, {'template_name':'logout.html'}),
    
    # url(r'^curso1/', include('curso1.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^encuestas/', include('encuestas.urls')),
)
